import 'dart:convert';

//recibe el json en forma de string y regresa una instancia de este modelo(ProductModel)
ProductModel productModelFromJson(String str) =>
    ProductModel.fromJson(json.decode(str));

// Toma el modelo y lo genera a un JSON
String productModelToJson(ProductModel data) => json.encode(data.toJson());

class ProductModel {
  String id;
  String titulo;
  double valor;
  bool disponible;
  String fotoUrl;

  ProductModel({
    this.id,
    this.titulo = '',
    this.valor = 0.0,
    this.disponible = true,
    this.fotoUrl,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        id: json["id"],
        titulo: json["titulo"],
        valor: json["valor"],
        disponible: json["disponible"],
        fotoUrl: json["fotoUrl"],
      );

  Map<String, dynamic> toJson() => {
        // "id": id,
        "titulo": titulo,
        "valor": valor,
        "disponible": disponible,
        "fotoUrl": fotoUrl,
      };
}
